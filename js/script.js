
// appel de cartesEnfant.JSON
function callChildData(choice){
  $.post("data/cartesEnfant.json",function(data){
    cartesEnfant = data
    tabTemp(cartesEnfant,choice)
  },"json")
}
// appel de cartesEnfant.JSON
function callTeenData(choice){
  $.post("data/cartesAdo.json",function(data){
    cartesAdo = data
    tabTemp(cartesAdo,choice)
  },"json")
}
// appel de cartesAdult.JSON
function callAdultData(choice){
  $.post("data/cartesAdult.json",function(data){
    cartesAdult = data
    tabTemp(cartesAdult,choice)
  },"json")
}
// récupération des cartes x2 dans un tableau temporaire
function tabTemp(tab,choice){
  var tabTemp = []
  var count = 0
  do{
    for (var i = 0; i < choice; i++) {
      // console.log(tab[i])
      tabTemp.push(tab[i])
    }
    count ++
  }while(count != 2)
  gameTab(tabTemp)
}

// création du tableau de cartes en ordre aleatoire pour le jeu
function gameTab(tab){
  var i, j, tmp
  for (var i = 0; i < tab.length; i++) {
    j= Math.floor(Math.random()*(i+1))
    tmp = tab[i]
    tab[i] = tab[j]
    tab[j] = tmp
  }
  affichage(tab)
}
// compte le nombre de coup jouer
function countCoup(){
  var count = 0
  if($(".returned").length==2){
    count ++
    console.log(count)
  }
}

// affichage des cartes du tableau aleatoire
function affichage(tab){
  // console.log($("#game .row").html())
  var target = $("#game .row")
  target.empty()
  $(tab).each(function(){
    var cartes = '<div class="carte"><div class="front"><img src="style/img/cartes/dos.png"></div><div class="back"><img src="'+this.url+'" data-id="'+this.id+'"></div></div>'
    target.append(cartes)
  })
  sizeCarte()
  returnCard()
  startingStars()
}
// definition de la taille des cartes en fonction de leur nombres
function sizeCarte(){
  var nbCarte = $(".carte").length
  var target = $(".carte")
  switch (nbCarte){
    case 6 :
      // 3x2
      target.addClass("col-sm-4")
      target.css("height","40vh")
      break;
    case 8 :
      // 4x2
      target.addClass("col-sm-3")
      target.css("height","35vh")
      break;
    case 12 :
      // 4x3
      target.addClass("col-sm-3")
      target.css("height","30vh")
      break;
    case 16 :
      // 4x4
      target.addClass("col-sm-3")
      target.css("height","20vh")
      break;
    case 20 :
      // 4x5
      target.addClass("col-sm-3")
      target.css("height","15vh")
      break;
    case 24 :
      // 4x6
      target.addClass("col-sm-2")
      target.css("height","20vh")
      break;
    case 36 :
      // 6x6
      target.addClass("col-sm-2")
      target.css("height","15vh")
      break;
  }
}

// retourne la carte au click
function returnCard(){
  $(".carte").click(function(){
    $(this).toggleClass("returned")
    winOrLoose()
  })
}

// vérification apres 2 cartes retournées
function winOrLoose (){
  var tabCarteId = []

  if($(".returned").length==2){
    countCoup ++
    // creer un tableau et insert l'id des carte dedans
    $(".returned>.back").each(function(){
      var carteId = $(this).children("img").attr("data-id")
      tabCarteId.push(carteId);
    })
    // parcours le tableau et compare les données
    for (var i = 0; i < tabCarteId.length; i++) {
      if(tabCarteId[i] == tabCarteId[i-1]){
        // console.log("prout")
        $(".returned").addClass("win")
        $(".win").removeClass("returned")
        winAstar()
        addStar()
        congratulation(countCoup)
      }
      if(tabCarteId[i] != tabCarteId[i-1]){
        setTimeout(function(){
          $(".returned").removeClass("returned")
        },1000)
      }
    }
  }
}
// lance l'animation de l'etoile gagnée
function winAstar(){
  setTimeout(function(){
    $("#winAStar>i").addClass("starWin")
  },500)
  setTimeout(function(){
    $("#winAStar>i").removeClass("starWin")
  },1500)
}

// definir si la partie est gagnée
function congratulation(countCoup){
  var nbWin = $(".win").length
  var nbCarte = $(".carte").length
  $("#nbCoup").empty()
  if(nbWin == nbCarte){
    setTimeout(function(){
      $(".gameWin").addClass("open")
      console.log(countCoup)
      var statJeu = 'Nombre de coups joués : '+countCoup
      $("#nbCoup").append(statJeu)
    },1600)
  }
}

// definir le nombre d'etoiles par rapport aux nombre de carte/2
function startingStars(){
  $("#stars").empty()
  var emptyStars = '<i class="far fa-star"></i>'
  var fillStars = '<i class="fas fa-star"></i>'
  for (var i = 0; i < $(".carte").length/2; i++) {
    $("#stars").append(emptyStars)
  }
}

// definir le nombre d'etoiles pleines et vides
function addStar(){
  setTimeout(function(){
    $("#stars").empty()
    $("#gameWinStars").empty()
    var nbWin = $(".win").length/2
    var nbCarte = $(".carte").length/2
    var nbEmptyStar = nbCarte - nbWin
    var emptyStars = '<i class="far fa-star"></i>'
    var fillStars = '<i class="fas fa-star"></i>'
    for (var i = 0; i < nbWin; i++) {
      $("#stars").append(fillStars)
      $("#gameWinStars").append(fillStars)
    }
    for (var i = 0; i < nbEmptyStar; i++) {
      $("#stars").append(emptyStars)
    }
  },1450)

}

// définition de l'age et du niveaux et verif de champs choisient
function select(){
  $("#jouer").click(function(){
    var selectAge = $("#age").val()
    var selectLevel = $("#level").val()
    var erreur = $("#errorMes")
    erreur.empty()
    erreur.css("border","none")
    // console.log(selectLevel)
    switch (selectAge){
      case "0":
        if(selectLevel == "0"){
          var mesSpe = "Veuillez choisir un âge et un niveau"
          var message = '<i class="fas fa-exclamation-triangle"></i>'+mesSpe+'<i class="fas fa-exclamation-triangle"></i>'
          erreur.append(message)
          erreur.css("border-bottom","1px solid red")
        }
        if(selectLevel == "1" || selectLevel == "2" || selectLevel == "3"){
          var mesSpe = "Veuillez choisir un âge"
          var message = '<i class="fas fa-exclamation-triangle"></i>'+mesSpe+'<i class="fas fa-exclamation-triangle"></i>'
          erreur.append(message)
          erreur.css("border-bottom","1px solid red")
        }
        break
      case "1":
        if(selectLevel == "0"){
          var mesSpe = "Veuillez choisir un niveau"
          var message = '<i class="fas fa-exclamation-triangle"></i>'+mesSpe+'<i class="fas fa-exclamation-triangle"></i>'
          erreur.append(message)
          erreur.css("border-bottom","1px solid red")
        }
        if(selectLevel == "1"){
          var nbcarte= 6/2
          callChildData(nbcarte)
          restartChild(nbcarte)
          $(".accueil").addClass("slideLeftAccueil")
        }
        if(selectLevel == "2"){
          var nbcarte= 8/2
          callChildData(nbcarte)
          restartChild(nbcarte)
          $(".accueil").addClass("slideLeftAccueil")
        }
        if(selectLevel == "3"){
          var nbcarte= 12/2
          callChildData(nbcarte)
          restartChild(nbcarte)
          $(".accueil").addClass("slideLeftAccueil")
        }

        break
      case "2":
        if(selectLevel == "0"){
          var mesSpe = "Veuillez choisir un niveau"
          var message = '<i class="fas fa-exclamation-triangle"></i>'+mesSpe+'<i class="fas fa-exclamation-triangle"></i>'
          erreur.append(message)
          erreur.css("border-bottom","1px solid red")
        }
        if(selectLevel == "1"){
          var nbcarte= 12/2
          callTeenData(nbcarte)
          restartTeen(nbcarte)
          $(".accueil").addClass("slideLeftAccueil")
        }
        if(selectLevel == "2"){
          var nbcarte= 16/2
          callTeenData(nbcarte)
          restartTeen(nbcarte)
          $(".accueil").addClass("slideLeftAccueil")
        }
        if(selectLevel == "3"){
          var nbcarte= 20/2
          callTeenData(nbcarte)
          restartTeen(nbcarte)
          $(".accueil").addClass("slideLeftAccueil")
        }
        break
      case "3":
        if(selectLevel == "0"){
          var mesSpe = "Veuillez choisir un niveau"
          var message = '<i class="fas fa-exclamation-triangle"></i>'+mesSpe+'<i class="fas fa-exclamation-triangle"></i>'
          erreur.append(message)
          erreur.css("border-bottom","1px solid red")
        }
        if(selectLevel == "1"){
          var nbcarte= 20/2
          callAdultData(nbcarte)
          restartAdult(nbcarte)
          $(".accueil").addClass("slideLeftAccueil")
        }
        if(selectLevel == "2"){
          var nbcarte= 24/2
          callAdultData(nbcarte)
          restartAdult(nbcarte)
          $(".accueil").addClass("slideLeftAccueil")
        }
        if(selectLevel == "3"){
          var nbcarte= 36/2
          callAdultData(nbcarte)
          restartAdult(nbcarte)
          $(".accueil").addClass("slideLeftAccueil")
        }
        break
    }
  })
}

// slide up accueil
function slideleft(){
  $("#jouer").click(function(){
    $(".accueil").addClass("slideLeftAccueil")
  })
}
// slide down accueil
function slideright(){
  $("#home").click(function(){
    $(".accueil").removeClass("slideLeftAccueil")
  })
  $("#winhome").click(function(){
    $(".accueil").removeClass("slideLeftAccueil")
    $(".gameWin").removeClass("open")
  })
}

// relancer le jeu
function restartChild(nbCarte){
  $("#rejouer").click(function(){
    countCoup = 0
    callChildData(nbCarte)
  })
  $("#winrejouer").click(function(){
    countCoup = 0
    callChildData(nbCarte)
    $(".gameWin").removeClass("open")
  })
}
// relancer le jeu
function restartTeen(nbCarte){
  $("#rejouer").click(function(){
    countCoup = 0
    callTeenData(nbCarte)
  })
  $("#winrejouer").click(function(){
    countCoup = 0
    callTeenData(nbCarte)
    $(".gameWin").removeClass("open")
  })
}
// relancer le jeu
function restartAdult(nbCarte){
  $("#rejouer").click(function(){
    countCoup = 0
    callAdultData(nbCarte)
  })
  $("#winrejouer").click(function(){
    countCoup = 0
    callAdultData(nbCarte)
    $(".gameWin").removeClass("open")
  })
}

// programme principal
$(function(){
  countCoup = 0
  select()
  slideright()
})
